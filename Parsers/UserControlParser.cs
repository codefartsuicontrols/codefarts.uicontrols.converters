﻿namespace Codefarts.UIControls.Converter.Parsers
{
    using System;
    using System.Xml;

    using Codefarts.UIControls;
    using Codefarts.XAMLGui.Editor.Code;
                              

    public class UserControlParser : IXamlParserPlugin
    {
        public Control Process(XMLParser parser, XmlElement xmlElement)
        {
            if (xmlElement.Name != "UserControl")
            {
                return null;
            }

            // get class attribute
            XmlAttribute classAttr = null;
            foreach (XmlAttribute attribute in xmlElement.Attributes)
            {
                if (attribute.LocalName == "Class")
                {
                    classAttr = attribute;
                    break;
                }
            }

            if (classAttr == null)
            {
                return null;
            }

            var type = Type.GetType(classAttr.Value);
            if (type != null)
            {
                try
                {
                    var control = AppDomain.CurrentDomain.CreateInstanceAndUnwrap(type.Assembly.FullName, type.FullName);
                    return control as Control;
                }
                catch 
                {
                      return null;
                }   
            }

            return null;
        }

        //private object GetClassTemplate(string nameSpace, string className)
        //{
        //    var template = "namespace {0}\r\n" +
        //                   "{\r\n" +
        //                   "    public class {1}" +
        //                   "    {\r\n" +
        //                   "        {2}\r\n" +
        //                   "    }\r\n" +
        //                   "}\r\n" ;

        //    return string.Format() template

        //}
    }
}