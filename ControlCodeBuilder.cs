﻿namespace Codefarts.XAMLGui.Editor.Code
{
    using System;
    using System.Collections.Generic;

    using Codefarts.UIControls;

    public class ControlCodeBuilder
    {
        public string Result { get; private set; }

        private bool isInitilized;

        private Dictionary<string, List<IControlCodeBuilderPlugin>> plugins;

        /// <summary>
        /// Initializes a new instance of the <see cref="ControlCodeBuilder"/> class.
        /// </summary>
        public ControlCodeBuilder()
        {
            this.plugins = new Dictionary<string, List<IControlCodeBuilderPlugin>>();
        }

        public void Initialize()
        {
            // search for types in each assembly that implement the IEditorTool interface
            var fullName = typeof(IControlCodeBuilderPlugin).FullName;
            var list = new List<IControlCodeBuilderPlugin>();

            // search through al assemblies
            foreach (var asm in AppDomain.CurrentDomain.GetAssemblies())
            {
                // search through all types
                foreach (var type in asm.GetTypes())
                {
                    // ignore abstract classes
                    if (type.IsAbstract)
                    {
                        continue;
                    }

                    // get interfaces that the type implements
                    foreach (var inter in type.GetInterfaces())
                    {
                        try
                        {
                            if (string.CompareOrdinal(inter.FullName, fullName) == 0)
                            {
                                var obj = asm.CreateInstance(type.FullName);
                                var instance = obj as IControlCodeBuilderPlugin;
                                list.Add(instance);
                            }
                        }
                        catch (Exception ex)
                        {
                            // ignore error
                         //   Debug.LogError(string.Format("Problem loading '{0}' as a control builder plugin.", type.FullName));
                        //    Debug.LogException(ex);
                        }
                    }
                }
            }

            foreach (var plugin in list)
            {
                if (!this.plugins.ContainsKey(plugin.Language))
                {
                    this.plugins.Add(plugin.Language, new List<IControlCodeBuilderPlugin>());
                }

                this.plugins[plugin.Language].Add(plugin);
            }

            //foreach (var tool in this.toolList)
            //{
            //    tool.Startup(this.editorApplication);
            //}       

            this.isInitilized = true;
        }

        public string[] Languages
        {
            get
            {
                var items = new string[this.plugins.Count];
                this.plugins.Keys.CopyTo(items, 0);
                return items;
            }
        }

        public void Build(Control control, string language)
        {
            if (!this.isInitilized)
            {
                throw new Exception("Not yet initialized.");
            }

            if (string.IsNullOrEmpty(language))
            {
                throw new ArgumentNullException("language");
            }

            if (control == null)
            {
                throw new ArgumentNullException("control");
            }

            var items = this.plugins[language];
            if (items == null)
            {
                throw new Exception("Language not supported.");
            }

            foreach (var plugin in items)
            {
                if (plugin.ControlType == control.GetType())
                {
                    var result = plugin.Build(control);
                    this.Result = result;
                    break;
                }
            }
        }
    }
}