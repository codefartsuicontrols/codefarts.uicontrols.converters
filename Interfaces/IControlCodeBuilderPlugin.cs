﻿namespace Codefarts.XAMLGui.Editor.Code
{
    using System;

    using Codefarts.UIControls;

    public interface IControlCodeBuilderPlugin
    {
        string Language { get; set; }

        Type ControlType { get; set; }

        string Build(Control control);
    }
}