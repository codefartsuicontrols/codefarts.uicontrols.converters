﻿namespace Codefarts.XAMLGui.Editor.Code
{
    using System.Xml;

    using Codefarts.UIControls;

    public interface IXamlParserPlugin
    {
        Control Process(XMLParser parser, XmlElement xmlElement);
    }
}