﻿namespace Codefarts.XAMLGui.Editor.Code
{
    using System.Collections.Generic;
    using System.IO;
    using System.Xml;

    using Codefarts.UIControls;


    public class XMLParser
    {
      //  private bool isInitilized;

        private List<IXamlParserPlugin> plugins;

        public List<IXamlParserPlugin> Plugins
        {
            get
            {
                return this.plugins;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="XMLParser"/> class.
        /// </summary>
        public XMLParser()
        {
            this.plugins=new List<IXamlParserPlugin>();
        }

        /*
        public void Initialize()
        {
            // search for types in each assembly that implement the IEditorTool interface
            var fullName = typeof(IXamlParserPlugin).FullName;
            var list = new List<IXamlParserPlugin>();

            // search through al assemblies
            foreach (var asm in AppDomain.CurrentDomain.GetAssemblies())
            {
                // search through all types
                foreach (var type in asm.GetTypes())
                {
                    // ignore abstract classes
                    if (type.IsAbstract)
                    {
                        continue;
                    }

                    // get interfaces that the type implements
                    foreach (var inter in type.GetInterfaces())
                    {
                        try
                        {
                            if (string.CompareOrdinal(inter.FullName, fullName) == 0)
                            {
                                var obj = asm.CreateInstance(type.FullName);
                                var instance = obj as IXamlParserPlugin;
                                list.Add(instance);
                            }
                        }
                        catch (Exception ex)
                        {
                            // ignore error
                            Debug.LogError(string.Format("Problem loading '{0}' as a xaml parser plugin.", type.FullName));
                            Debug.LogException(ex);
                        }
                    }
                }
            }

            // added created tool to the tool list
            this.plugins = new List<IXamlParserPlugin>(list);

            //foreach (var tool in this.toolList)
            //{
            //    tool.Startup(this.editorApplication);
            //}       

            this.isInitilized = true;
        }
         */

        public void Build(Stream stream)
        {
            //if (!this.isInitilized)
            //{
            //    throw new Exception("Not yet initialized.");
            //}

            var document = new XmlDocument();
            document.Load(stream);

            this.Build(document);
        }

        public void Build(XmlDocument document)
        {
            Control rootControl = null;
            foreach (var plugin in this.plugins)
            {
                var result = plugin.Process(this, document.DocumentElement);
                if (result != null)
                {
                    rootControl = result;
                    break;
                }
            }

            this.Result = rootControl;
        }

        public Control Result { get; private set; }
    }
}
